<html>
<head>
    <title>Wunder</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootswatch/4.5.2/yeti/bootstrap.min.css" integrity="sha384-mLBxp+1RMvmQmXOjBzRjqqr0dP9VHU2tb3FK6VB0fJN/AOu7/y+CAeYeWJZ4b3ii" crossorigin="anonymous">
</head>
<body>
    <div class="container">
        <div class="py-5 text-center">
            <img class="d-block mx-auto mb-4" src="https://d33wubrfki0l68.cloudfront.net/efa63294c9786fd61e8a501b092b46123a4340e7/ce1ad/uploads/global/wunder-mobility-black.svg" alt="" height="72">
            <h2>Customer Registration</h2>
        </div>
        <div class="row">
            <div class="col-md-12 order-md-1">
                @if(isset($_COOKIE['uuid']) && $_COOKIE['uuid'] != '' && isset($_COOKIE['currentStep']))
                {{ $page =  $_COOKIE['currentStep'] }}
                @endif
                @switch($page)
                @case('payment')
                <form id='customerRegistration' method="post" action="{{ route('crteateCustomer').'/'.$_COOKIE['uuid'] }}" class="needs-validation" novalidate="">
                    <input type="hidden" name="_method" value="put" />
                    <h4 class="mb-3">Payment Information</h4>
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <label for="accountOwner">Account owner</label>
                            <input type="text" class="form-control" id="accountOwner" name="accountOwner" placeholder="" required="">
                            <small class="text-muted">Full name</small>
                            <div class="invalid-feedback">
                                Name on card is required
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="iban">IBAN</label>
                            <input type="text" class="form-control" id="iban" name="iban" placeholder="" required="">
                            <div class="invalid-feedback">
                                Credit card number is required
                            </div>
                        </div>
                    </div>
                    <input name="_step" type="hidden" value="payment" />
                    {{-- <input name="_token" type="hidden" value="{{ csrf_token() }}"/> --}}
                    <hr class="mb-4">
                    <button class="btn btn-primary btn-lg btn-block" type="submit">Complete</button>
                </form>
                @break

                @case('address')
                <form id='customerRegistration' method="post" action="{{ route('crteateCustomer').'/'.$_COOKIE['uuid'] }}" class="needs-validation" novalidate="">
                    <input type="hidden" name="_method" value="put" />
                    <h4 class="mb-3">Address</h4>
                    <div class="d-block my-3">
                        <div class="row">
                            <div class="col-md-6 mb-3">
                                <label for="street">Street</label>
                                <input type="text" class="form-control" id="street" name="street" placeholder="Main St" required="">
                                <div class="invalid-feedback">
                                    Please enter your shipping address.
                                </div>
                            </div>

                            <div class="col-md-6 mb-3">
                                <label for="houseNumber">House Number</label>
                                <input type="text" class="form-control" id="houseNumber" name="houseNumber" placeholder="Building #" required="">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 mb-3">
                                <label for="city">City</label>
                                <input type="text" class="form-control" id="city" name="city" placeholder="" required="">
                                <div class="invalid-feedback">
                                    Please provide a valid city.
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="zipCode">Zip</label>
                                <input type="text" class="form-control" id="zipCode" name="zipCode" placeholder="" required="">
                                <div class="invalid-feedback">
                                    Zip code required.
                                </div>
                            </div>
                        </div>
                    </div>
                    <input name="_step" type="hidden" value="address" />
                    {{-- <input name="_token" type="hidden" value="{{ csrf_token() }}"/> --}}
                    <hr class="mb-4">
                    <button class="btn btn-primary btn-lg btn-block" type="submit">Continue</button>
                </form>
                @break
                @case('personal')
                @default
                <form id='customerRegistration' method="post" action="{{ route('crteateCustomer') }}" class="needs-validation" novalidate="">
                    <h4 class="mb-3">Personal Information</h4>
                    <div class="d-block my-3">
                        <div class="row">
                            <div class="col-md-6 mb-3">
                                <label for="firstName">First name</label>
                                <input type="text" class="form-control" id="firstName" name="firstName" placeholder="" value="" required="" style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABHklEQVQ4EaVTO26DQBD1ohQWaS2lg9JybZ+AK7hNwx2oIoVf4UPQ0Lj1FdKktevIpel8AKNUkDcWMxpgSaIEaTVv3sx7uztiTdu2s/98DywOw3Dued4Who/M2aIx5lZV1aEsy0+qiwHELyi+Ytl0PQ69SxAxkWIA4RMRTdNsKE59juMcuZd6xIAFeZ6fGCdJ8kY4y7KAuTRNGd7jyEBXsdOPE3a0QGPsniOnnYMO67LgSQN9T41F2QGrQRRFCwyzoIF2qyBuKKbcOgPXdVeY9rMWgNsjf9ccYesJhk3f5dYT1HX9gR0LLQR30TnjkUEcx2uIuS4RnI+aj6sJR0AM8AaumPaM/rRehyWhXqbFAA9kh3/8/NvHxAYGAsZ/il8IalkCLBfNVAAAAABJRU5ErkJggg==&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%;">
                                <div class="invalid-feedback">
                                    Valid first name is required.
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="lastName">Last name</label>
                                <input type="text" class="form-control" id="lastName" name="lastName" placeholder="" value="" required="">
                                <div class="invalid-feedback">
                                    Valid last name is required.
                                </div>
                            </div>
                            <div class="col-md-12 mb-3">
                                <label for="telephone">Phone</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">#</span>
                                    </div>
                                    <input type="text" class="form-control" id="telephone" name="telephone" placeholder="Phone" required="">
                                    <div class="invalid-feedback" style="width: 100%;">
                                        Your phone is required.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input name="_step" type="hidden" value="personal" />
                    {{-- <input name="_token" type="hidden" value="{{ csrf_token() }}"/> --}}
                    <hr class="mb-4">
                    <button class="btn btn-primary btn-lg btn-block" type="submit">Continue</button>
                </form>
                @break
                @endswitch

            </div>
        </div>
        <footer class="my-5 pt-5 text-muted text-center text-small">
            <p class="mb-1">© 2020-2021 Wunder Mobility</p>
        </footer>
    </div>
    <script>

    </script>
</body>
</html>
