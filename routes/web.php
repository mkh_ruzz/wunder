<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', [
    'uses' => 'PageController@index'
]);

$router->get('pages/{page}', [
    'uses' => 'PageController@pages'
]);

$router->get('pages', [
    'as'=> 'pages',
    'uses' => 'PageController@pages'
]);

$router->group(['prefix' => 'api'], function () use ($router) {
    $router->get('customers',  [ 'as'=> 'showAllCustomers', 'uses' => 'CustomerController@showAllCustomers']);

    $router->get('customers/{id}', [ 'as'=> 'showOneCustomer' ,'uses' => 'CustomerController@showOneCustomer']);

    $router->post('customers', [ 'as'=> 'crteateCustomer' ,'uses' => 'CustomerController@create']);

    $router->delete('customers/{id}', [ 'as'=> 'deleteCustomer' ,'uses' => 'CustomerController@delete']);

    $router->put('customers/{id}', [  'as'=> 'updateCustomer' ,'uses' => 'CustomerController@update']);
  });
