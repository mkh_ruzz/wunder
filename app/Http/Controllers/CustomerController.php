<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Cookie;
use GuzzleHttp\Client;

class CustomerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    //

    public static function showAllCustomers()
    {
        return response()->json(Customer::all());
    }

    public static function showOneCustomer($id)
    {
        return response()->json(Customer::find($id));
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'firstName' => 'required',
            'lastName' => 'required',
            'telephone' => 'required|unique:users'
        ]);

        $customer = Customer::create($request->all());
        if ($request->get('_step') && $request->get('_step') == 'personal') {
            return redirect()->to(route('pages') . '/address')->withCookie(new Cookie('uuid', $customer['uuid']))->withCookie(new Cookie('currentStep', 'address'));
        }
        return response()->json($customer, 201);
    }

    public function update($id, Request $request)
    {
        $customer = Customer::findOrFail($id);
        $customer->update($request->all());

        if ($customer['accountOwner'] && $customer['iban']) {
            try {
                $client = new Client();
                $paymentResponse = $client->request('POST', 'https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data', [
                    "headers" => [
                        'Content-Type' => 'application/json'
                    ],
                    "body" => json_encode([
                        'customerId' =>  $customer['uuid'],
                        'iban' => $customer['iban'],
                        'owner' => $customer['accountOwner']
                    ])
                ]);
                $paymentInformation = json_decode($paymentResponse->getBody());
                $customer->paymentDataId = $paymentInformation->paymentDataId;
                $customer->save();
            } catch (Exception $e) {
                // Add Logs
            }
        }

        if ($request->get('_step') && $request->get('_step') == 'address') {
            return redirect()->to(route('pages') . '/payment')->withCookie(new Cookie('currentStep', 'payment'));
        }

        if ($request->get('_step') && $request->get('_step') == 'payment') {
            return redirect()->to(route('pages') . '/success')->withCookie(new Cookie('currentStep', 'success'))->withCookie(new Cookie('uuid', ''));
        }

        return response()->json($customer, 200);
    }

    public function delete($id)
    {
        Customer::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}
