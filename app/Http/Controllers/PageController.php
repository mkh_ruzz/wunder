<?php

namespace App\Http\Controllers;

class PageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    //

    public static function index()
    {
        return self::pages('index');
    }

    public static function pages($page)
    {
        // TODO: Redierect wrong urls 404,403 and index access from
        return view('index')->with('page', $page);
    }
}
