Wunder PHP Take Home Test
==============

datbase structure database\wunder.sql

As it would be beneficiarry to use MVI to enhance abstraction or MVVM to enhance isolation and extendability, but I went here with MVP for simplicity and used some of the ideas from both MVI and MVMM to enhance my code but not fully adabting to them. 

Performance Tweaks & General Enhancements
------------
- Add Loggers, and Benchmarks
- Add Unit testing
- Add generic and specic handlers for both Http and php exceptions
- Update payemnetDataID with same transaction as general update
- Front End Validation integration with backend
- Redierect wrong urls 404,403

Installation
------------
First, you will need to install [Composer](http://getcomposer.org/) following the instructions on their site.

Then, simply run the following command:

```sh
composer install
```

Configuration
-------------
Make sure to define your database connection in `.env`, then run the provided migration:

```sh
php artisan migrate
```

Now you can test your app! Just set your redirect_uri to `http://localhost:8000/auth` and run a PHP server:

```sh
php artisan serve
```

or by running it in your preferred server htdocs
