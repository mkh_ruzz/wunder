<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
                $table->uuid('uuid')->primary();
                $table->string('firstName');
                $table->string('lastName');
                $table->string('email')->nullable();
                $table->string('telephone')->unique();
                $table->string('street')->nullable();
                $table->string('houseNumber')->nullable();
                $table->string('zipCode')->nullable();
                $table->string('city')->nullable();
                $table->string('type');
                $table->string('password')->nullable();
                $table->string('iban')->nullable();
                $table->string('accountOwner')->nullable();
                $table->string('paymentDataId')->nullable();
                $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
